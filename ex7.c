/* ex7.c
   This code made available under the CC0 license.
   http://creativecommons.org/publicdomain/zero/1.0/
   
   To compile this code, type:
   
   make ex7
   
   To run the resulting program, type:
   
   ./ex7
*/
#include <stdio.h>
#include <stdlib.h>

int main () {
  FILE *fp = fopen("book-of-needlework.txt", "r");
  
  /* Declare a variable called 'ch' to hold a character. */
  /* Declare a pointer to an array of integers called 'vowels'. */
  /* Allocate space for five integers in the array. */

  /* Loop through the file */
    /* Count the number of each vowel you encounter. 
       Store the count of the letter 'a' in the zeroth element
       of the vowels array, 'e' in the first element of the array, and
       so on.
      */  
  
  /* Print the number of vowels of each type you encountered */
  
  /* Print a newline */

  return 0;
}
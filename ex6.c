/* ex6.c
   This code made available under the CC0 license.
   http://creativecommons.org/publicdomain/zero/1.0/
   
   To compile this code, type:
   
   make ex6
   
   To run the resulting program, type:
   
   ./ex6
*/
#include <stdio.h>
#include <stdlib.h>

int main () {
  FILE *fp = fopen("book-of-needlework.txt", "r");
  
  /* Declare a variable called 'ch' to hold a character. */
  /* Declare a variable called 'vowels' to hold an integer. */

  /* Loop through the file */
    /* Count the number of vowels you encounter */
  
  /* Print the number of vowels you encountered */
  
  /* Print a newline */

  return 0;
}